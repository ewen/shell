express = require 'express'
io = require 'socket.io'

stylesheets = require './modules/stylesheets'
scripts = require './modules/scripts'

config = require './config'

app = express.createServer()

stylesheets.add href: 'http://yui.yahooapis.com/combo?3.4.1/build/cssreset/cssreset-min.css&3.4.1/build/cssfonts/cssfonts-min.css&3.4.1/build/cssbase/cssbase-min.css'
stylesheets.add href: 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/smoothness/jquery-ui.css'
stylesheets.add href: 'stylesheets/main.css'
scripts.add src: 'http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js'
scripts.add src: 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js'
scripts.add src: 'scripts/main.js'

app.configure ->
    app.set 'view engine', 'jade'
    app.set 'view options', 
        stylesheets: stylesheets.list()
        scripts: scripts.list()
    app.use express.static __dirname + '/public'
    return

app.get '/', (req, res) ->
    res.render 'index'
    return

module.exports = app