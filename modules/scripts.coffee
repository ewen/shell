scripts = []

exports.list = -> scripts

exports.add = (spec) ->
    scripts.push
        src: spec.src || ''
        type: spec.type || 'text/javascript'
    return