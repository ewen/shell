stylesheets = []

exports.list = -> stylesheets

exports.add = (spec) ->
    stylesheets.push
        href: spec.href || ''
        media: spec.media || 'screen'
        type: spec.type || 'text/css'
        rel: spec.rel || 'stylesheet'
    return